//
//  ViewController.m
//  ObjCScoreKeeper
//
//  Created by Kelson Vella on 9/19/17.
//  Copyright © 2017 Disre. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UITextField *Team1Txt;
@property (strong, nonatomic) IBOutlet UITextField *Team2Txt;

@property (strong, nonatomic) IBOutlet UILabel *team1Lbl;
@property (strong, nonatomic) IBOutlet UILabel *team2Lbl;

@property (strong, nonatomic) IBOutlet UIStepper *team1Stepper;
@property (strong, nonatomic) IBOutlet UIStepper *team2Stepper;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     //Do any additional setup after loading the view, typically from a nib.
    _Team1Txt.delegate = self;
    _Team2Txt.delegate = self;
}

- (IBAction)ResetTouched:(UIButton *)sender {
    _Team1Txt.text = @"";
    _Team2Txt.text = @"";
    _team1Lbl.text = @"0";
    _team2Lbl.text = @"0";
    _team1Stepper.value = 0;
    _team2Stepper.value = 0;
}
- (IBAction)team1StepperChanged:(UIStepper *)sender {
    int value = sender.value;
    self.team1Lbl.text = [NSString stringWithFormat:@"%d", value];
}

- (IBAction)team2StepperChanged:(UIStepper *)sender {
    int value = sender.value;
    self.team2Lbl.text = [NSString stringWithFormat:@"%d", value];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
